﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04
{
    class CaesarCipher
    {
        public static void Resolve()
        {
            Console.WriteLine("Masukan inputan: ");
            string kata = Console.ReadLine();

            Console.WriteLine("Perpindahan: ");
            int pindah = Convert.ToInt32(Console.ReadLine());

            int[] karakter = new int[kata.Length];

            for (int i = 0; i < kata.Length; i++)
            {
                karakter[i] = (int)kata[i];

                if (karakter[i] > 64 && karakter[i] < 91)
                {
                    pindah = (karakter[i] - 65) % 26;
                    karakter[i] = 65 + pindah;
                }

                else if (karakter[i] > 96 && karakter[i] < 123)
                {
                    pindah = (karakter[i] - 97) % 26;
                    karakter[i] = 97 + pindah;
                }

                //hasilRotate[i] = (char)alpha[i];
                Console.Write((char)karakter[i]);
            }

            Console.WriteLine();


        }
    }
}
