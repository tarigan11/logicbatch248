﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04
{
    class Program
    {
        static void Main(string[] args)
        {
            string answer = "Y";

            while (answer.ToUpper() == "Y")
            {
                Console.WriteLine("Masukan nomor soal : ");
                int nomorSoal = int.Parse(Console.ReadLine());

                switch (nomorSoal)
                {
                    case 1:
                        Console.WriteLine("01. Camel Case ");
                        Console.WriteLine();
                        CamelCase.Resolve();
                        break;

                    case 2:
                        Console.WriteLine("02. Strong Password");
                        Console.WriteLine();
                        StrongPassword.Resolve();
                        break;

                    case 3:
                        Console.WriteLine("03.  Caesar Cipher");
                        Console.WriteLine();
                        CaesarCipher.Resolve();
                        break;

                    case 4:
                        Console.WriteLine("04. Mars Exploration");
                        Console.WriteLine();
                        MarsExploration.Resolve();
                        break;

                    case 5:
                        Console.WriteLine("05. HackerRank In A String");
                        Console.WriteLine();
                        HackerRankInAString.Resolve();
                        break;

                    case 6:

                        Console.WriteLine("06. Pangrams");
                        Console.WriteLine();
                        Pangrams.Resolve();
                        break;

                    case 7:
                        Console.WriteLine("07. Separate the Numbers");
                        Console.WriteLine();
                        SeparatetheNumbers.Resolve();
                        break;

                    case 8:
                        Console.WriteLine("08. Gemstones");
                        Console.WriteLine();
                        Gemstones.Resolve();
                        break;

                    case 9:
                        Console.WriteLine("09. Making Anagrams");
                        Console.WriteLine();
                        MakingAnagrams.Resolve();
                        break;

                    case 10:
                        Console.WriteLine("10. Two Strings");
                        Console.WriteLine();
                        TwoStrings.Resolve();
                        break;

                    case 11:
                        Console.WriteLine("11. Middel Asterisk");
                        Console.WriteLine();
                        MiddelAsterisk.Resolve();
                        break;

                    case 12:
                        Console.WriteLine("12. Palindrome");
                        Console.WriteLine();
                        Palindorme.Resolve();
                        break;

                    default:
                        Console.WriteLine("Soal tidak ditemukan");
                        break;
                }

                Console.WriteLine("Lanjutkan?");
                answer = Console.ReadLine();
                Console.WriteLine();
            }
        }
    }
}
