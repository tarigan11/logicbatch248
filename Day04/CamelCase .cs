﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04
{
    class CamelCase
    {
        public static void Resolve()
        {
            Console.WriteLine("Masukan CamelCase: ");
            string camelCase = Console.ReadLine();
            char[] cameCaseChar = camelCase.ToCharArray();
            string hasil = "";
            int banyakKata = 1;

            if (char.IsLower(cameCaseChar[0]))
            {
                for (int i = 0; i < camelCase.Length; i++)
                {
                    if (char.IsUpper(cameCaseChar[i]))
                    {
                        hasil += (" " + cameCaseChar[i]);
                        banyakKata++;
                    }
                    else if (char.IsLower(cameCaseChar[i]))
                    {
                        hasil += (cameCaseChar[i]);
                    }
                }

                Console.WriteLine();
                Console.WriteLine(hasil);

                Console.WriteLine();
                Console.WriteLine("Banyak objek = " + banyakKata);
            }
            else
            {
                Console.WriteLine("Bukan Kalimat camelCase");
            }
        }
    }
}
