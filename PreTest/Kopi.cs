﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreTest
{
    class Kopi
    {
        public static void Resolve()
        {
            Console.WriteLine("Saldo OPO:");
            int saldoOPO = int.Parse(Console.ReadLine());

            double diskon = 0.5;
            double cashbacPerPercentage = 0.1;
            double temp = saldoOPO;
            int cup = 0;
            double totalBelanja = 0;
            double cashback = 0;

            if (saldoOPO < 40000)
            {
                while (temp < 40000 && temp >= 18000)
                {
                    if (temp >= 18000)
                    {
                        temp -= 18000;
                        cup++;
                    }
                }

                cashback = saldoOPO - temp;
                cashback = cashback * cashbacPerPercentage;
                temp += cashback;
                Console.WriteLine("Jumlah cup = " + cup);
                Console.WriteLine("Saldo Akhir = " + temp);

            }
            else
            {
                while (temp > (18000 * diskon))
                {
                    temp -= (18000 * diskon);
                    cup++;
                }

                totalBelanja = saldoOPO - temp;
                if (totalBelanja > 100000)
                {
                    Console.WriteLine("Tidak jadi membeli");
                }

                else
                {
                    cashback = totalBelanja * cashbacPerPercentage;
                    if (cashback > 30000)
                    {
                        cashback = 30000;
                    }
                    temp += cashback;
                    Console.WriteLine("Jumlah cup = " + cup);
                    Console.WriteLine("Saldo Akhir = " + temp);
                }

            }


        }
    }
}
