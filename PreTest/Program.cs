﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreTest
{
    class Program
    {
        static void Main(string[] args)
        {
            string answer = "Y";

            while (answer.ToUpper() == "Y")
            {
                Console.WriteLine("Masukan nomor soal : ");
                int nomorSoal = int.Parse(Console.ReadLine());

                switch (nomorSoal)
                {
                    case 1:
                        Console.WriteLine("01. Pulsa");
                        Console.WriteLine();
                        Pulsa.Resolve();
                        break;

                    case 2:
                        Console.WriteLine("02. Bensin");
                        Console.WriteLine();
                        Bensin.Resolve();
                        break;

                    case 3:
                        Console.WriteLine("03. Konversi Volume");
                        Console.WriteLine();
                        KonversiVolume.Resolve();
                        break;

                    case 4:
                        Console.WriteLine("04. Kopi");
                        Console.WriteLine();
                        Kopi.Resolve();
                        break;

                    case 5:
                        Console.WriteLine("05.Nasigoreng");
                        Console.WriteLine();
                        Nasigoreng.Resolve();
                        break;

                    case 6:

                        Console.WriteLine("06.Permaianan");
                        Console.WriteLine();
                        Permainan.Resolve();
                        break;

                    case 7:
                        Console.WriteLine("07. Keranjang");
                        Console.WriteLine();
                        Keranjang.Resolve();
                        break;

                    case 8:
                        Console.WriteLine("08. Stor Tunai");
                        Console.WriteLine();
                        SetorTunai.Resolve();
                        break;

                    case 9:
                        Console.WriteLine("09. Kartu");
                        Console.WriteLine();
                        Kartu.Resolve();
                        break;

                    case 10:
                        Console.WriteLine("10. Penjumlahan Kedua Deret");
                        Console.WriteLine();
                        PenjumlahanKeduaDeret.Resolve();
                        break;

                    case 11:
                        Console.WriteLine("11. Number One");
                        Console.WriteLine();
                        NumberOne.Resolve();
                        break;

                    case 12:
                        Console.WriteLine("12. Eviternity Numbers");
                        Console.WriteLine();
                        EviternityNumbers.Resolve();
                        break;

                    default:
                        Console.WriteLine("Soal tidak ditemukan");
                        break;
                }

                Console.WriteLine("Lanjutkan?");
                answer = Console.ReadLine();
            }
        }
    }
}
