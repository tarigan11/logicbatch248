﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day05
{
    class Utility
    {


        public static int[] ConvertStringToIntArry(string numbers)
        {
            string[] stringNumbersArray = numbers.Split(' ');
            int[] numberArray = new int[stringNumbersArray.Length];

            // Convert to int
            for (int i = 0; i < numberArray.Length; i++)
            {
                numberArray[i] = int.Parse(stringNumbersArray[i]);
            }

            return numberArray;
        }

        public static int Sum(int number1, int number2)
        {

            int total = number1 + number2;
            return total;
        }


        public static int SumTotal(int total1, int total2)
        {

            int total = total1 + total2;
            return total;
        }

    }
}
