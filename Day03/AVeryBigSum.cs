﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class AVeryBigSum
    {
        public static void Resolve()
        {
            Console.WriteLine("Masukan deret angka: ");
            string numbers = Console.ReadLine();
            long[] numbersArray = Utility.ConvertStringToLongArry(numbers);

            long total = numbersArray.Sum();
            Console.WriteLine(total);

            Console.ReadKey();
        }
    }
}
